# Official Python runtime as a parent image base on alpine
FROM python:3.11-alpine3.18

# Install Requirments
ADD django101_app/requirements.txt /app/requirements.txt

# Add bash for debuging
RUN apk add --no-cache bash

#Install all dependencies
RUN pip install --upgrade pip
RUN pip install -r /app/requirements.txt

# App's directory in the container
ADD django101_app /app
WORKDIR /app

# Exposed port of the Django Application  
EXPOSE 8001 

# runserver
ENTRYPOINT ["python3"] 
CMD ["manage.py", "runserver", "0.0.0.0:8001"]

# Start server with gunicorn (test)
# CMD ["gunicorn", "--bind", ":8001", "--workers", "3", "django101_app.wsgi"]